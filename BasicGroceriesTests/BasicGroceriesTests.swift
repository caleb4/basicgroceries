//
//  BasicGroceriesTests.swift
//  BasicGroceriesTests
//
//  Created by Caleb on 4/28/17.
//  Copyright © 2017 Caleb M. All rights reserved.
//

import XCTest
@testable import BasicGroceries
import Alamofire

class BasicGroceriesTests: XCTestCase {
    
    var vc: ViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard(name: "Main",
                                      bundle: Bundle.main)
        let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        vc = navigationController.topViewController as! ViewController
        
        UIApplication.shared.keyWindow!.rootViewController = vc
        
        // Test and Load the View at the Same Time!
        XCTAssertNotNil(navigationController.view)
        XCTAssertNotNil(vc.view)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCart() {
        XCTAssert(vc.cart.subtotal == 0.00)
        vc.cart.items.append(vc.items[1])
        XCTAssert(vc.cart.subtotal == vc.items[1].price)
        let cartTableVC = CartTableViewController()
        vc.navigationController?.pushViewController(cartTableVC, animated: true)
        XCTAssertNotNil(cartTableVC.view)
        XCTAssertNotNil(cartTableVC.tableView)
    }
    
    func testCurrencyService() {
        var currencies = vc.currencyService.fetchCurrencies()
        XCTAssert(currencies.count > 0)
        vc.currencyService.deleteCurrencyData()
        currencies = vc.currencyService.fetchCurrencies()
        XCTAssert(currencies.count < 1)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
