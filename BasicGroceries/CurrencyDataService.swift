//
//  CurrencyDataService.swift
//  BasicGroceries
//
//  Created by Caleb McGuire on 5/1/17.
//  Copyright © 2017 Caleb M. All rights reserved.
//

import Foundation
import CoreData
import Alamofire

class CurrencyDataService: NSObject {
    
    static let sharedInstance = CurrencyDataService()
    let networkManager = NetworkReachabilityManager()
    let accessToken = "?access_key=82dda6f78c6f7f60e4aedad359a8d15f"
    let baseURL = "http://apilayer.net/api/"
    let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
    lazy var managedObjectContext: NSManagedObjectContext? = {
        return self.appDelegate.persistentContainer.viewContext
    }()
    dynamic var currencies: [Currency] = []
    
    private override init() {}
    
    func getCurrencyData() {
        self.currencies = self.fetchCurrencies()
        let usdRateURL = baseURL + "live" + accessToken
        
        networkManager?.listener = { status in
            if (self.networkManager?.isReachable)! {
                Alamofire.request(usdRateURL).responseJSON { response in
//                    print(response.request as Any)  // original URL request
//                    print(response.response as Any) // HTTP URL response
                    print(response.data as Any)     // server data
                    print(response.result)   // result of response serialization
                    
                    guard let result = response.result.value as? [String : Any],
                        let quotes = result["quotes"] as? [String : Any]
                        else {
                            print("Malformed data received from currency service")
                            return
                    }
                    
                    //Since new currencies can be added to the service - just delete existing local currency data and get fresh data.
                    self.deleteCurrencyData()
                    guard let managedObjectContext = self.managedObjectContext
                        else {
                            return
                    }
                    //USD Values
                    if let usdCurrency = NSEntityDescription.insertNewObject(forEntityName: "Currency", into: managedObjectContext) as? Currency {
                        usdCurrency.name = "USD"
                        usdCurrency.rate = 1
                    }
                    for value in quotes {
                        let name = value.key.replacingOccurrences(of: "USD", with: "")
                        if let rate = value.value as? Double {
                            
                            if let currency = NSEntityDescription.insertNewObject(forEntityName: "Currency", into: managedObjectContext) as? Currency {
                                currency.name = name
                                currency.rate = rate
                            }
                        }
                    }
                    self.appDelegate.saveContext()
                    self.currencies = self.fetchCurrencies()
                }
            }
        }
        networkManager?.startListening()
    }
    
    func deleteCurrencyData() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Currency")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try self.managedObjectContext?.execute(deleteRequest)
        } catch let error as NSError {
            // TODO: handle the error
            print("Failed to delete existing currencies: \(error)")
        }
        self.currencies = []
    }
    
    func fetchCurrencies() -> [Currency] {
        let currenciesFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Currency")
        let sort = NSSortDescriptor(key: "name", ascending: true)
        currenciesFetch.sortDescriptors = [sort]
        do {
            guard let fetchedCurrencies = try self.managedObjectContext?.fetch(currenciesFetch) as? [Currency]
                else {
                    return []
                }
            return fetchedCurrencies
        } catch {
            // TODO: handle the error
            print("Failed to fetch currencies: \(error)")
            return []
        }
    }
    
}
