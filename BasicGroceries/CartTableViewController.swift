//
//  CartTableViewController.swift
//  BasicGroceries
//
//  Created by Caleb McGuire on 5/2/17.
//  Copyright © 2017 Caleb M. All rights reserved.
//

import UIKit

class CartTableViewController: UITableViewController {

    let cart = Cart.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return cart.items.count
        } else {
            return 1
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartItemCell", for: indexPath)
        if indexPath.section == 0 {
            cell.textLabel?.text = cart.items[indexPath.row].name
            cell.detailTextLabel?.text = String(format: "%.2f", (cart.items[indexPath.row].price * cart.multiplier))
        } else {
            cell.textLabel?.text = "Total:"
            cell.detailTextLabel?.text = String(format: "%.2f", (cart.subtotal * cart.multiplier))
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: cell.textLabel!.font.pointSize)
            cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: cell.detailTextLabel!.font.pointSize)
        }
        
        return cell
    }

}
