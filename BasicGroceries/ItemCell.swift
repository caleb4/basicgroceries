//
//  ItemCell.swift
//  BasicGroceries
//
//  Created by Caleb McGuire on 5/2/17.
//  Copyright © 2017 Caleb M. All rights reserved.
//

import UIKit

class ItemCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var emojiLabel: UILabel!
    
}
