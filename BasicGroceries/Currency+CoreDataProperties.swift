//
//  Currency+CoreDataProperties.swift
//  BasicGroceries
//
//  Created by Caleb on 4/30/17.
//  Copyright © 2017 Caleb M. All rights reserved.
//

import Foundation
import CoreData

extension Currency {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Currency> {
        return NSFetchRequest<Currency>(entityName: "Currency")
    }

    @NSManaged public var name: String?
    @NSManaged public var rate: Double
    @NSManaged public var friendlyName: String?
    
}
