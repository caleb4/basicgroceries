//
//  ViewController.swift
//  BasicGroceries
//
//  Created by Caleb on 4/28/17.
//  Copyright © 2017 Caleb M. All rights reserved.
//

import UIKit

class ViewController: UICollectionViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {

    let currencyService = CurrencyDataService.sharedInstance
    let cart = Cart.sharedInstance
    var items: [Item] = []
    fileprivate let sectionInsets = UIEdgeInsets(top: 2.0, left: 2.0, bottom: 2.0, right: 2.0)
    fileprivate let itemsPerRow: CGFloat = 3
    @IBOutlet weak var currencyTextField: UITextField!
    var currencyPicker: UIPickerView?
    var indexOfUSD = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currencyService.addObserver(self, forKeyPath: "currencies", options: .new, context: nil)
        addItems()
        currencyService.getCurrencyData()
    }
    
    deinit {
        currencyService.removeObserver(self, forKeyPath: "currencies")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "currencies" {
            if let newValue = change?[.newKey] as? [Currency] {
                if newValue.count > 0 {
                    if let index = newValue.index(where: {$0.name == "USD"}) {
                        self.indexOfUSD = index
                    }
                    if currencyPicker != nil {
                        currencyPicker?.reloadAllComponents()
                    } else {
                        setupPickerView()
                    }
                    currencyTextField.delegate = self
                    currencyTextField.isHidden = false
                    currencyTextField.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    func addItems() {
        let milk = Item(name: "Bottle of Milk", price: 1.30, emoji: "🥛")
        let peas = Item(name: "Bag of Peas", price: 0.95, emoji: "👝")
        let eggs = Item(name: "Dozen Eggs", price: 2.10, emoji: "🥚")
        let beans = Item(name: "Can of Beans", price: 0.73, emoji: "🍈")
        
        items = [milk, peas, eggs, beans]
    }
    
    func setupPickerView() {
        currencyPicker = UIPickerView()
        currencyPicker?.dataSource = self
        currencyPicker?.delegate = self
        currencyTextField.inputView = currencyPicker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.blue
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let defaultButton = UIBarButtonItem(title: "Default", style: UIBarButtonItemStyle.plain, target: self, action: #selector(defaultPicker))
        
        toolBar.setItems([doneButton, spaceButton, defaultButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        currencyTextField.inputAccessoryView = toolBar
        currencyPicker?.selectRow(indexOfUSD, inComponent: 0, animated: true)
    }
    
    // MARK: - Picker toolbar button methods
    func donePicker() {
        if let index = currencyPicker?.selectedRow(inComponent: 0) {
            currencyTextField.text = currencyService.currencies[index].name
            cart.multiplier = currencyService.currencies[index].rate
        }
        currencyTextField.resignFirstResponder()
        self.collectionView?.reloadData()
    }
    
    func defaultPicker() {
        currencyPicker?.selectRow(indexOfUSD, inComponent: 0, animated: true)
        donePicker()
    }
    
    // MARK: - Selected item helper.
    func isItemSelected(item: Item) -> Bool {
        if cart.items.contains(where: {$0 === item}) {
            return true
        } else {
            return false
        }
    }
    
    // MARK: - UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    // MARK: - Picker view DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencyService.currencies.count
    }
    
    // MARK: - Picker view Delegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let title = currencyService.currencies[row].name
        return title
    }
    
    // MARK: - UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedCell = collectionView.cellForItem(at: indexPath) as! ItemCell
        
        if cart.items.contains(where: {$0 === items[indexPath.row]}) {
            if let index = cart.items.index(where: {$0 === items[indexPath.row]}) {
                cart.items.remove(at: index)
                selectedCell.contentView.alpha = 1.0
                selectedCell.isSelected = false
            }
        } else {
            cart.items.append(items[indexPath.row])
            selectedCell.contentView.alpha = 0.5
            selectedCell.isSelected = true
        }
    }
}

// MARK: - UICollectionViewDataSource
extension ViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: indexPath) as! ItemCell
        cell.nameLabel.text = items[indexPath.row].name
        cell.priceLabel.text = String(format: "%.2f", items[indexPath.row].price * cart.multiplier)
        cell.emojiLabel.text = items[indexPath.row].emoji
        if isItemSelected(item: items[indexPath.row]) {
            cell.contentView.alpha = 0.5
        } else {
            cell.contentView.alpha = 1.0
        }
        return cell
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
