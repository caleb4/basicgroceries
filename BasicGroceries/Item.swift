//
//  Item.swift
//  BasicGroceries
//
//  Created by Caleb on 4/29/17.
//  Copyright © 2017 Caleb M. All rights reserved.
//

import Foundation

class Item {
    var name: String
    var price: Double
    var emoji: String
    
    init(name: String, price: Double, emoji: String) {
        self.name = name
        self.price = price
        self.emoji = emoji
    }
}
