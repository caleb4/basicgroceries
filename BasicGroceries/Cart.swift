//
//  Cart.swift
//  BasicGroceries
//
//  Created by Caleb McGuire on 5/2/17.
//  Copyright © 2017 Caleb M. All rights reserved.
//

import Foundation

class Cart {
    static let sharedInstance = Cart()
    var items: [Item] = []
    var subtotal: Double {
        var t = 0.00
        for item in items {
            t += item.price
        }
        return t
    }
    var multiplier: Double = 1
    private init() {}
}
