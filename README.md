# README #

### What is this repository for? ###

* Quick summary
    - It allows the user to add and remove basic grocery items in a shopping cart and then proceed to the check out view.  If the user has a valid network connection - the app will download currency data and the user is then able to tap in the top left of the main view to change the currency displayed. If the app was unable to retrieve the currency data - the field in the top left ("USD") will not show.

    - Possible improvements - Update UI design, add button in app to allow user to refresh the currency data whenever they tap it. Additionally - could utilize the other endpoint to get the friendly names of each currency. The unit tests coverage for business logic is decent - though more UITests could be added.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
    - The app uses Alamofire to monitor the connection status and make requests. CoreData is used to persist the currency data.
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact